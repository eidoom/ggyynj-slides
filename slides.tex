\documentclass{beamer}

\usetheme{Singapore}

\usepackage{caption}
\usepackage{siunitx}

\usepackage[
    bibstyle=authoryear,
    citestyle=authoryearsquare,
]{biblatex}
\addbibresource{slides.bib}
\renewcommand*{\bibfont}{\tiny}

\graphicspath{{./img/}}

\usefonttheme[onlymath]{serif}

\title{NLO QCD corrections to diphoton-plus-jet production through gluon fusion at hadron colliders}
\author{Ryan Moodie}
\institute{Institute for Particle Physics Phenomenology \\\vspace{1ex} Durham University}
\date{Internal seminar \\\vspace{1ex} 14 Jan 2022}
\titlegraphic{
    \includegraphics[width=10em,height=10em,keepaspectratio]{ippp_logo}
    %\includegraphics[width=10em,height=10em,keepaspectratio]{du_logo}
}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section*{Introduction}

\begin{frame}{Precision frontier}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Precision QCD
                    \begin{itemize}
                        \item Indirect probe of new physics by small deviations from SM
                    \end{itemize}
                \item Modern collider experiments demand increasing precision
                \item Percent-level QCD theory predictions
                \item Requires NNLO
                    \begin{align*}
                        \alpha_s&\approx 0.1\\
                        \mathrm{d}\sigma&=\mathrm{d}\sigma^{\mathrm{LO}}+\alpha_s \mathrm{d}\sigma^{\mathrm{NLO}} + {\alpha_s}^2 \mathrm{d}\sigma^{\mathrm{NNLO}}
                    \end{align*}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{CMSjets}
                \captionsetup{labelformat=empty}
                \caption{\href{https://cds.cern.ch/record/2114784}{CMS}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Diphoton final states}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Important background for measuring properties of the Higgs
                    \begin{itemize}
                        \item Rich kinematics of $2\to3$ offer attractive probes
                    \end{itemize}
                \item Gluon-initiated channel is excellent testing ground for new technologies
                \vspace{-1em}
                    \begin{itemize}
                        \item Loop-induced
                            \begin{itemize}
                                \item Simpler pole structure
                                \item Challenges conventional phase space optimisations
                            \end{itemize}
                        \item Simpler colour structure
                    \end{itemize}
                \item NNLO $pp\to\gamma\gamma j$ convergence poorest where gluon-fusion dominant \parencite{Chawdhry:2021hkp}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{gg1l}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Outline}{NLO QCD corrections to $gg\to\gamma\gamma g$}
    \tableofcontents
\end{frame}

\section{Virtual correction}

\begin{frame}{Outline}{NLO QCD corrections to $gg\to\gamma\gamma g$}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Virtual QCD corrections to $gg\to\gamma\gamma g$ amplitude}
    \begin{itemize}
        \item \parencite{Badger:2021imn}
        \item Analytic two-loop full-colour five-point amplitudes
        \item Reconstruct finite remainders from numerical evaluations over finite fields
        \item Pentagon function basis
        \item Fast and stable \texttt{C++} implementation provided in \texttt{NJet} library
            \url{https://bitbucket.org/njet/njet}
    \end{itemize}
\end{frame}

% make this a flow chart or similar
\begin{frame}{Computing loop corrections}
    \begin{enumerate}
        \item Generate all Feynman diagrams
            \begin{itemize}
                    %     \item \texttt{QGRAF}
                \item 50 at one loop and 1527 at two loop
            \end{itemize}
        \item Write the integrands
            \begin{itemize}
                \item Feynman rules, process algebra
                    %     \item Numerator algebra: \texttt{FORM}, \texttt{Mathematica}, \texttt{Spinney}
                    %     \item Assign integral topology by denominator structure
            \end{itemize}
        \item Reduce to scalar integrals
            \begin{itemize}
                \item Solve large linear system of IBP identities
            \end{itemize}
        \item Map to master integrals
            \begin{itemize}
                \item Differential equations
            \end{itemize}
        \item Subtract poles to obtain finite remainders
        \item Expand to special function basis
            \begin{equation*}
                A = \sum_i r_i f_i
            \end{equation*}
        \item Optimise form of $r_i$
            \begin{itemize}
                \item Analytic reconstruction, partial fractioning
            \end{itemize}
    \end{enumerate}
\end{frame}

\begin{frame}{Amplitude decomposition}
    \begin{itemize}
        \item Colour and loops
            \begin{multline*}
                \mathcal{A}(1_g,2_g,3_g,4_\gamma,5_\gamma) = \\
                g_s\, g_e^2 \sum_{q=1}^{N_f} Q_q^2\, f^{a_1 a_2 a_3} \sum_{\ell=1}^{\infty} \left(n_\epsilon \frac{\alpha_s}{4\pi} \right)^{\ell} A^{(\ell)}(1_g,2_g,3_g,4_\gamma,5_\gamma)
            \end{multline*}
        \item Powers of $N_c$ and $N_f$
            \begin{align*}
                A^{(1)}(1_g,2_g,3_g,4_\gamma,5_\gamma) =\,& A^{(1)}_1(1_g,2_g,3_g,4_\gamma,5_\gamma) \\
                A^{(2)}(1_g,2_g,3_g,4_\gamma,5_\gamma) =\,& N_c A^{(2)}_1(1_g,2_g,3_g,4_\gamma,5_\gamma) \,+ \\
                &\frac{1}{N_c} A^{(2)}_2(1_g,2_g,3_g,4_\gamma,5_\gamma) \,+ \\
                &N_f A^{(2)}_3(1_g,2_g,3_g,4_\gamma,5_\gamma)
            \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}{Integral topologies}
    \begin{figure}
        \includegraphics[width=\textwidth]{integral_topologies}
    \end{figure}
    \begin{itemize}
        \item Four independent integral families
        \item Surprisingly, only LC contains non-planar integrals
            \begin{itemize}
                \item Opposite pattern to quark-initiated channels
                \item See this by attaching photons to three-gluon diagrams
            \end{itemize}
    \end{itemize}
    \vspace{1em}
    \begin{columns}
        \begin{column}{0.19\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{colour_diagram1}
                \captionsetup{labelformat=empty}
                \caption{$N_c$}
            \end{figure}
        \end{column}
        \begin{column}{0.19\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{colour_diagram2}
                \captionsetup{labelformat=empty}
                \caption{$N_c$}
            \end{figure}
        \end{column}
        \begin{column}{0.19\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{colour_diagram3}
                \captionsetup{labelformat=empty}
                \caption{$N_c$}
            \end{figure}
        \end{column}
        \begin{column}{0.19\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{colour_diagram4}
                \captionsetup{labelformat=empty}
                \caption{$\frac{1}{N_c}$}
            \end{figure}
        \end{column}
        \begin{column}{0.19\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{colour_diagram5}
                \captionsetup{labelformat=empty}
                \caption{$N_c - \frac{1}{N_c}$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Finite fields}
    \begin{itemize}
        \item Large intermediate expressions: algebraic complexity
        \item Bypass with numerical evaluation over finite fields
            \begin{align*}
                n \to n \;\textrm{mod}\; p
            \end{align*}
        \item Integer obtained by Chinese Remainder Theorem
        \item Use rational numbers modulo a large prime number
            \begin{align*}
                r = \frac{a}{b} \to r \,\textrm{mod}\, p = (a\times(b^{-1}\;\mathrm{mod}\;p)) \;\mathrm{mod}\; p
            \end{align*}
        \item Avoids catastrophic cancellation of floats
        \item Used for all intermediate steps
        \item \texttt{FiniteFlow} \parencite{Peraro:2019svx}
    \end{itemize}
\end{frame}

% \begin{frame}{Reconstruction}
%     \begin{itemize}
%         \item Analytic reconstruction from numerical evaluations over finite fields
%         \item Univariate partial fractioning
%     \end{itemize}
% \end{frame}

\begin{frame}{Implementation}
    \begin{itemize}
        \item Finite remainders coded up in \texttt{NJet}
            \begin{itemize}
                \item Special functions from \texttt{PentagonFunctions++}
                \item Sparse matrix algebra by \texttt{Eigen3}
                \item Extended precision through \texttt{QD}
            \end{itemize}
        \item 6 independent helicity amplitudes analytically permuted to 16 mostly-plus helicities
        \item Construct partial amplitudes as
            \begin{equation*}
                F^{h} = c^h_{i} M^h_{ij} f^h_j
            \end{equation*}
            \vspace{-1em}
            \begin{itemize}
                \item $f^h_j$ indexes pentagon function monomial list (global)
                \item $M^h_{ij}$ rational sparse matrices (partial)
                \item $c^h_{i}$ independent rational coefficients in momentum twistor variables (helicity)
            \end{itemize}
        \item Mostly-minus configurations by complex conjugation of coefficients and parity flipping special functions
    \end{itemize}
\end{frame}

\begin{frame}{Evaluation strategy}
    \begin{enumerate}
        \item Phase-space point deformed to ensure masslessness and momentum conservation to float precision
        \item \texttt{f64/f64} evaluation with dimension scaling test, checking accuracy $>$ threshold
            \begin{itemize}
                \item Measures rounding errors
            \end{itemize}
        \item If fails, recompute coefficients in \texttt{f128} (\texttt{f128/f64})
        \item If fails, recompute special functions in \texttt{f128} (\texttt{f128/f128})
    \end{enumerate}
\end{frame}

\begin{frame}{Stability and test validation}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{stability}
            \end{figure}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{euler}
                \captionsetup{labelformat=empty}
                \caption{Negative test results}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item Compare direct \texttt{f128/f128} to strategy with 4 digit cutoff
        \item \num{60000} points over \texttt{NNLOJET} one-loop phase space
        \item Most false positives have true error near cut: effect small
        \item Always anomalies in extreme phase spaces
    \end{itemize}
\end{frame}

\begin{frame}{Timing}
    \begin{itemize}
        \item Evaluate over \num{100000} points of ``physical'' phase space
        \item Mean timing per phase space point:
            \begin{itemize}
                \item \texttt{f64/f64}: \SI{9}{\second}
                    \begin{itemize}
                        \item \SI{99}{\percent} of time spent on special functions
                    \end{itemize}
                \item Evaluation strategy with three digit target: \SI{26}{\second}
            \end{itemize}
        \item Stability and speed show readiness for phenomenological application
    \end{itemize}
\end{frame}

\section{NLO cross-section}

\begin{frame}{Outline}{NLO QCD corrections to $gg\to\gamma\gamma g$}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{NLO QCD correction to $gg\to\gamma\gamma g$ cross-section}
    \begin{itemize}
        \item \parencite{Badger:2021ohm}
        \item First differential cross sections from five-point two-loop full-colour amplitudes for the LHC
        \item Although N$^3$LO in $pp\to\gamma\gamma j$, large gluon PDF amplifies contribution
        \item Observe significant NLO corrections
        \item Important ingredient to be combined with NNLO $pp\to\gamma\gamma j$
    \end{itemize}
\end{frame}

\begin{frame}{NLO construction}
    \begin{align*}
        \sigma_{gg\to \gamma\gamma g + X}^{\mathrm{LO}} =& \,\mathcal{O}(\alpha_s^3) \\
        \sigma_{gg\to \gamma\gamma g + X}^{\rm NLO} =&
        \int \mathrm{d}\Phi_3 \left|
        \raisebox{-6mm}{\includegraphics[width=17mm]{gg1l}}%
        \right|^2 + \nonumber\\&
        \int \mathrm{d}\Phi_3 \, 2 \, \mathrm{Re}\left(
        \raisebox{-6mm}{\includegraphics[width=17mm]{gg1l}}%
        ^\dagger\cdot
        \raisebox{-6mm}{\includegraphics[width=17mm]{gg2l}}%
        \right) + \nonumber\\&
        \int \mathrm{d}\Phi_4 \left|
        \raisebox{-7.5mm}{\includegraphics[width=17mm]{gg1lr}}%
        \right|^2 +
        \int \mathrm{d}\Phi_4 \left|
        \raisebox{-7.5mm}{\includegraphics[width=17mm]{ggqqyy}}%
        \right|^2
        + \mathcal{O}(\alpha_s^5)
    \end{align*}
    ($\gamma$ couple to internal quark loop)
\end{frame}

\begin{frame}{Amplitudes}
    \begin{itemize}
        \item $\mathcal{A}^{(2)}_{\mathrm{V}}$ from \texttt{NJet} (previous section)
            \begin{itemize}
                \item Per-mille accuracy threshold
            \end{itemize}
        \item $\mathcal{A}^{(1)}$ for $\mathrm{LO}$ and $\mathrm{R}$ from combination of \texttt{OpenLoops2} \parencite{Buccioni:2019sur} and \texttt{NJet}
            \begin{itemize}
                \item \texttt{OpenLoops2} generally more efficient
                \item \texttt{NJet} necessary for high precision evaluation of exceptional points
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Infrared regularisation}
    \begin{itemize}
        \item $\mathcal{A}^{(1)}_{\mathrm{LO}}$ and $\mathcal{A}^{(1)}_{\mathrm{R}}$ finite since loop-induced
        \item Renormalised $\mathcal{A}^{(2)}_{\mathrm{V}}$ IR divergent from loop integration
        \item $\sigma^{\mathrm{R}}$ IR singular for unresolved emissions
        \item Cancel by KLN theorem
        \item Use finite remainder of $\mathcal{A}_{\mathrm{V}}$
        \item Perform cancellation with antenna subtraction method \parencite{Currie:2013vh}
    \end{itemize}
\end{frame}

\begin{frame}{Cuts}
    Cuts are typical for \SI{13}{TeV} LHC
    \begin{itemize}
        \item $p_T(\gamma_1) > 30$ GeV
        \item $p_T(\gamma_2) > 18$ GeV
        \item $|\eta(\gamma\gamma)|<2.4$
        \item $m(\gamma\gamma) \geq 90$ GeV
        \item $p_{T}(\gamma\gamma) > 20$ GeV
        \item $\Delta R_{\gamma\gamma} > 0.4$
        \item Photons selected by smooth cone isolation
            \begin{itemize}
                \item $\Delta R_0 = 0.4$
                \item $E_T^{\rm max} = 10$ GeV
                \item $\epsilon=1$
            \end{itemize}
        \item No jet requirement for our observables as $p_{T}(\gamma\gamma)$ cut avoids all outgoing partons unresolved
    \end{itemize}
\end{frame}

\begin{frame}{Details}
    \begin{itemize}
        \item Parameters
            \begin{itemize}
                \item PDFs: NNLO set of \texttt{NNPDF3.1}
                \item $\alpha_{s}$ evaluated using \texttt{LHAPDF} with $\alpha_{s}\left(m_Z\right)=0.118$
                \item $\alpha=1/137.035999139$
            \end{itemize}
        \item Uncertainties
            \begin{itemize}
                \item MC errors $<1\%$ (not on plots)
                \item Theory uncertainty from seven-point variation of $\mu_F=\mu_R=\mu$ around dynamical central value:
                    \begin{itemize}
                        \item $\mu = \frac{1}{2}m_{T} = \frac{1}{2} \left( m^2(\gamma\gamma)+p_T^2(\gamma\gamma)\right)^{1/2}$
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Distributions 1}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{pt_g1g2_2}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{Mg1g2_2}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item NLO corrections often comparable in size to LO
        \item Corrections largest at low $p_{T}(\gamma\gamma)$ and $m(\gamma\gamma)$, and smoothly decreasing towards larger values
        \item Total cross section dominated by $p_T(\gamma\gamma)$ or $m(\gamma\gamma)$
    \end{itemize}
\end{frame}

\begin{frame}{Distributions 2}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{cosCS_2}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{dphi_2}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item Distributions differential only in geometrical photon variables
            \begin{itemize}
                \item Collins-Soper angle, azimuthal decorrelation
            \end{itemize}
        \item Near-uniform ratio of 2
        \item No overlap of uncertainties
    \end{itemize}
\end{frame}

\begin{frame}{Distributions 3}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{dy_2}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{yg1g2_2}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item Distributions differential only in geometrical photon variables
            \begin{itemize}
                \item Rapidity different, total rapidity
            \end{itemize}
        \item LO and NLO absolute scale uncertainties appear comparable
        \item Relative uncertainty reduced from 50\% at LO to 30\% at NLO
    \end{itemize}
\end{frame}

\begin{frame}{Doubly-differential distributions 1}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{2D_cosCS_mgg}
            \end{figure}
            \begin{itemize}
                \item NLO/LO decreasing with $m(\gamma\gamma)$
                \item Uniform in $\left|\phi_{CS}(\gamma\gamma)\right|$
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{2D_ptgg_ygg}
            \end{figure}
            \begin{itemize}
                \item NLO/LO decreasing with $p_T(\gamma\gamma)$
                \item More pronouned at forward rapidity than central
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Doubly-differential distributions 2}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{2D_mgg_ptgg_bins}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{2D_mgg_ptgg_cuts}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item Non-uniform shape in $m(\gamma\gamma)$ in highest $p_T$ bin indicates logarithmic corrections in $\log(m(\gamma\gamma)/p_T(\gamma\gamma))$
    \end{itemize}
\end{frame}

\begin{frame}{Takeaway}
    \begin{itemize}
        \item Find significant NLO corrections
        \item Strongest at low $p_{T}(\gamma\gamma)$ and $m(\gamma\gamma)$
        \item Demonstrates importance of combining gluon-induced and quark-induced diphoton-plus-jet signatures for LHC
    \end{itemize}
\end{frame}

\section{Amplitude neural networks}

\begin{frame}{Outline}{NLO QCD corrections to $gg\to\gamma\gamma g$}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Amplitude neural networks for $gg\to\gamma\gamma gg$}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item \url{https://eidoom.gitlab.io/acat2021}
                \item \parencite{Aylett-Bullock:2021hmo}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{gg1lr}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section*{Conclusion}

\begin{frame}{Conclusion}
    \begin{itemize}
        \item NLO QCD corrections to diphoton-plus-jet production through gluon fusion
            \begin{itemize}
                \item Well motivated for LHC phenomenology
                \item Success of advances in QCD theory
            \end{itemize}
        \item Complete full-colour two-loop $gg\to\gamma\gamma g$ finite remainder
            \begin{itemize}
                \item Efficient and stable evaluation over physical scattering region
            \end{itemize}
        \item Fully differential distributions in the NLO cross-section
            \begin{itemize}
                \item Significant NLO corrections
                \item Important contribution for precision studies of diphoton-plus-jet production
            \end{itemize}
        \item Amplitude neural networks provide general framework to optimise high-multiplicity observables
            \begin{itemize}
                \item Applicable to real correction of $gg\to\gamma\gamma g$
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Bibliography}
    \printbibliography
\end{frame}

\end{document}
