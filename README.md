# [ggyynj-slides](https://gitlab.com/eidoom/ggyynj-slides)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7763103.svg)](https://doi.org/10.5281/zenodo.7763103)

[Live here](https://eidoom.gitlab.io/ggyynj-slides/slides.pdf)

Some images stolen from [`gitlab:eidoom/feynman-diagram-template`](https://gitlab.com/eidoom/feynman-diagram-template) and cited papers.

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
